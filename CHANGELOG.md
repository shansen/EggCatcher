= Changelog =

== 2.5 ==

* Added DeleteVillagerInventoryOnCatch option.

== 2.4 ==

* Updated for 1.9 support.

== 2.3 ==

* Fixed configuration files resets on reboot.

== 2.2 ==

* Added support for 1.8 entity types.

== 2.1.1 ==

* Changed update method to be compliant with new bukkit guidelines.

== 2.1 ==

* Built and verified against Bukkit 1.7
* Built and verified against Vault 1.2.27
* Horses will now drop their inventory when caught.
* Fixed prevention of baby capturing doesn't work on villager. 
* Added permission to allow free catches despite costs in configuration (Credits: [[https://github.com/alexschrod|alexschrod]])

== 2.00 ==

* <<color red>>ONLY FOR CRAFTBUKKIT BUILD #2811 AND LATER!<</color>>
* <<color red>>Breaking change: Configuration file is overwritten!<</color>>
* Added support for horses
* Fixed bug where tamed animals could not be caught.
* Changed default setting for NonPlayerCatching to false.

== 1.29 ==

* Pigs with saddles will now drop their saddle when caught.

== 1.28 ==

* Adds support for data values on ItemCosts.  (Credits: andrepl)

== 1.27 ==
* Preserve entity names. (Credits: andrepl)
== 1.26 ==
* Updated and checked with Bukkit 1.5 dev.
== 1.25 ==
* It is now possible to configure how much health a mob must have lost in order to catch it.
* Fixed a bug where it was possible to spawn infinite amounts of chickens if the catch failed.
* Added update check
* Removed redundant enable/disable messages
== 1.24 ==
* GriefPrevention support is working again.
== 1.23 ==
* Added metrics
* Added cancellable EggCaptureEvent (Credits: andrepl)
== 1.22 ==
* Fixed problem with bats and witches.
* Added support for a target account for money withdrawn from catching.
== 1.21.1 ==
* Fix to configuration updater
== 1.21 ==
* Added 1.4 Support
* Support for bats.
* Support for witches.
== 1.19 ==
* Verified with Minecraft 1.3.1 and 1.3.2
== 1.18 ==
* Fixed problem where config file gets rewritten on every reload.
== 1.17 ==
* Fixed problem where the plugin would crash if Vault was installed without an economy plugin.
== 1.16 ==
* <<color red>>Breaking change: Configuration file is overwritten!<</color>>
* Ocelot support.
== 1.15 ==
* Various bug fixes
== 1.14 ==
* Chicken spawning is now configurable.
== 1.13 ==
* Configurable fix for sheared sheeps.
== 1.12 ==
* Configurable fix for tamable and baby animals.
== 1.11 ==
* Dispensers can now be used to catch mobs. (configurable)
* Vault support.
* Item cost support.
* Loose egg on fail can be configured.
== 1.10 ==
* Beta version
== 1.9 ==
* Various fixes
* Added a configurable smoke effect
== 1.8 ==
* Fail message no longer appears if a player throws an egg at another player.
== 1.7 ==
* Added a configurable explosion effect.
== 1.6 ==
* Permissions can now be disabled in the config file.
== 1.5 ==
* Updated permissions to lowercase to avoid confusion with certain permission plugins.
== 1.4 ==
* The catch chance can now be configured for the specific mobs.
* A success/fail message for catching a mob can now be configured.
== 1.3 ==
* Added DropChance to the configuration file.
== 1.2 ==
* Fixed MagmaCube
* Fixed MushroomCow
* Fixed CaveSpider
* Fixed PigZombie